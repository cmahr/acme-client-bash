#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. ./acme/api.sh
. ./config.sh

DOMAIN_CONFIG_FILE="$1"

if [[ ! -f "$DOMAIN_CONFIG_FILE" ]]; then
    echo "Abort: Could not find domain config file '$DOMAIN_CONFIG_FILE'"
    return 1
fi


acme.api.obtain_certificate "$ACME_ENDPOINT_DIRECTORY" \
    "$ACME_ACCOUNT_PRIVATE_KEY_FILE" \
    "$ACME_ACCOUNT_KEY_ID_FILE" \
    "$DOMAIN_CONFIG_FILE"
