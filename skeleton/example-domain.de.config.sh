# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# domain configuration file for acme bash client
# will be sourced in bash functions
# => use bash (local) variable syntax

local DOMAIN_COUNTRY="DE"
local DOMAIN_STATE="Bayern"
local DOMAIN_CITY="München"
local DOMAIN_ORGANIZATION_NAME=""
local DOMAIN_ORGANIZATION_UNIT=""
local DOMAIN_COMMON_NAME="example-domain.de"
local DOMAIN_EMAIL_ADDRESS="<YOUR_EMAIL_ADDRESS>"

local DOMAIN_SANS=("$DOMAIN_COMMON_NAME" "www.$DOMAIN_COMMON_NAME")

local DOMAIN_BASE_PATH="/projects/example-domain_de"
local DOMAIN_WELL_KNOWN_PATH="$DOMAIN_BASE_PATH/acme-challenge"

local DOMAIN_PRIVATE_KEY_FILE="$DOMAIN_BASE_PATH/ssl/private-key.pem"
local DOMAIN_CERTIFICATE_FILE="$DOMAIN_BASE_PATH/ssl/certificate.pem"
