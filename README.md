<!--
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
-->

# acme-client-bash
A simple and incomplete implementation of an ACME client (e.g., [Let's Encrypt](https://letsencrypt.org/)) primarily
built for teaching.

## Requirements
*acme-client-bash* is designed to run in a somewhat current Bash with rather minimal dependencies:
* awk
* base64
* curl
* grep
* openssl
* sed
* xxd

## Quickstart
1. Copy or clone this repository to your server.

2. Ensure all the aforementioned prerequisites are installed (or install them). Further ensure that your web server is
    configured to serve static files from the .well-known/acme-challenge directory.

3. Depending on whether you want to use the staging or production endpoint of Let's Encrypt, copy and modify the
    corresponding configuration skeleton:
    ```bash
    $ cp skeleton/production/config.sh .
    $ vim config.sh
    ```

   In particular, replace `<YOUR_EMAIL_ADDRESS>` with your valid email address.

4. Register an account with Let's Encrypt:
    ```bash
   $ . register_account.sh
   ```

5. Create a configuration file for the domain you want to serve via TLS from the domain configuration skeleton:
    ```bash
   $ mkdir -p domain
   $ cp skeleton/example-domain.de.config.sh domain/example-domain.de.config.sh
   $ vim domain/example-domain.de.config.sh
   ```

   In particular, replace `<YOUR_EMAIL_ADDRESS>` with your valid email address and adapt the domain common name as well
   as all paths to point to the correct locations.

6. Obtain a signed certificate for your domain:
    ```bash
   $ . obtain_certificate.sh domain/example-domain.de.config.sh 
   ```

7. Reload your web server to ensure the newly obtained certificate is used.

8. Enjoy your TLS-encrypted website :-)
