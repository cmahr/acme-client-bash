#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. ./acme/api.sh
. ./config.sh

if [[ ! -d "$ACME_ACCOUNT_BASE_PATH" ]]; then
    mkdir "$ACME_ACCOUNT_BASE_PATH"
    chmod 0700 "$ACME_ACCOUNT_BASE_PATH"
fi

if [[ ! -f "$ACME_ACCOUNT_PRIVATE_KEY_FILE" ]]; then
    openssl genrsa 4096 > "$ACME_ACCOUNT_PRIVATE_KEY_FILE"
    chmod 0600 "$ACME_ACCOUNT_PRIVATE_KEY_FILE"
fi

acme.api.register_account "$ACME_ENDPOINT_DIRECTORY" \
    "$ACME_ACCOUNT_PRIVATE_KEY_FILE" \
    "$ACME_ACCOUNT_KEY_ID_FILE" \
    "$ACME_ACCOUNT_EMAIL"
