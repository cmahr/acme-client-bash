#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. ./acme/api.sh
. ./config.sh

if [[ ! -d "$ACME_ACCOUNT_BASE_PATH" ]]; then
    echo "Abort: The accound base path '$ACME_ACCOUNT_BASE_PATH' is not a directory."
    return 1
fi

if [[ ! -f "$ACME_ACCOUNT_PRIVATE_KEY_FILE" ]]; then
    echo "Abort: Could not find account private key file '$ACME_ACCOUNT_PRIVATE_KEY_FILE'"
    return 2
fi

if [[ ! -f "$ACME_ACCOUNT_KEY_ID_FILE" ]]; then
    echo "Abort: Could not find account key ID file '$ACME_ACCOUNT_KEY_ID_FILE'"
    return 3
fi

acme.api.deactivate_account "$ACME_ENDPOINT_DIRECTORY" \
    "$ACME_ACCOUNT_PRIVATE_KEY_FILE" \
    "$ACME_ACCOUNT_KEY_ID_FILE"
