#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ACME_DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null && pwd )"

. "$ACME_DIRECTORY/request.sh" || true || . ./request.sh

acme.api.verify_prerequisites () {
    for prerequisite in "awk" "base64" "curl" "grep" "openssl" "sed" "xxd"; do
        command -v $prerequisite > /dev/null || { echo "Abort: '$prerequisite' not found!"; return 1; }
    done
}

acme.api.deactivate_account () {
    local DIRECTORY_ENDPOINT="$1"
    local ACCOUNT_PRIVATE_KEY_FILE="$2"
    local ACCOUNT_KEY_ID_FILE="$3"

    acme.api.verify_prerequisites

    local DIRECTORY_RESPONSE="$(acme.request.get_directory_response "$DIRECTORY_ENDPOINT")"
    local NONCE="$(acme.request.get_new_nonce "$DIRECTORY_RESPONSE")"
    local ACCOUNT_ENDPOINT

    if [[ -f "$ACCOUNT_KEY_ID_FILE" ]]; then
        ACCOUNT_ENDPOINT="$(head -n1 "$ACCOUNT_KEY_ID_FILE")"
    else
        local REGISTER_ENDPOINT="$(acme.request.get_endpoint "$DIRECTORY_RESPONSE" "newAccount")"

        local PAYLOAD_JSON='{"onlyReturnExisting":true}'

        local REGISTER_RESPONSE="$(acme.request.send "$REGISTER_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$PAYLOAD_JSON")"
        NONCE="$(acme.request.get_nonce "$REGISTER_RESPONSE")"
        ACCOUNT_ENDPOINT="$(acme.request.get_location "$REGISTER_RESPONSE")"
    fi

    local PAYLOAD_JSON='{"status":"deactivated"}'

    local ACCOUNT_RESPONSE="$(acme.request.send "$ACCOUNT_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$PAYLOAD_JSON")"

    echo "$(acme.http.extract_body "$ACCOUNT_RESPONSE")"

    # TODO: verify HTTP status code is 200
    # TODO: verify JSON status is "deactivated"
}

acme.api.obtain_certificate () {
    local DIRECTORY_ENDPOINT="$1"
    local ACCOUNT_PRIVATE_KEY_FILE="$2"
    local ACCOUNT_KEY_ID_FILE="$3"
    local DOMAIN_CONFIG_FILE="$4"

    acme.api.verify_prerequisites

    . "$DOMAIN_CONFIG_FILE"

    local DIRECTORY_RESPONSE="$(acme.request.get_directory_response "$DIRECTORY_ENDPOINT")"
    local NONCE="$(acme.request.get_new_nonce "$DIRECTORY_RESPONSE")"
    local NEW_ORDER_ENDPOINT="$(acme.request.get_endpoint "$DIRECTORY_RESPONSE" "newOrder")"

    local IDENTIFIERS="["
    for identifier in "${DOMAIN_SANS[@]}"; do
        IDENTIFIERS+="$(printf '{"type":"dns","value":"%s"},' "$identifier")"
    done
    IDENTIFIERS="${IDENTIFIERS::-1}]"

    local PAYLOAD_JSON="$(printf '{"identifiers":%s}' "$IDENTIFIERS")"

    echo "Sending new certificate order."
    local ORDER_RESPONSE="$(acme.request.send "$NEW_ORDER_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$PAYLOAD_JSON")"
    local NONCE="$(acme.request.get_nonce "$ORDER_RESPONSE")"

    local ORDER_RESPONSE_BODY="$(acme.http.extract_body "$ORDER_RESPONSE")"
    local ORDER_STATUS="$(acme.json.get_value "$ORDER_RESPONSE_BODY" "status")"
    echo "Order status is $ORDER_STATUS."

    if [[ "$ORDER_STATUS" == "invalid" ]]; then
        echo "Abort: Order status invalid."
        return 1
    fi

    if [[ "$ORDER_STATUS" == "pending" ]]; then
        echo "Iterate over pending authorizations!"
        local AUTHORIZATIONS="$(acme.json.get_simple_array_values "$ORDER_RESPONSE_BODY" "authorizations")"

        for AUTHORIZATION_ENDPOINT in $AUTHORIZATIONS; do
            echo "authorization: $AUTHORIZATION_ENDPOINT"

            local AUTHORIZATION_RESPONSE="$(acme.request.send "$AUTHORIZATION_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE")"
            NONCE="$(acme.request.get_nonce "$AUTHORIZATION_RESPONSE")"

            local AUTHORIZATION_STATUS="$(acme.request.get_authorization_status "$AUTHORIZATION_RESPONSE")"

            local RESPONSE_FILE_PATH
            while [[ "$AUTHORIZATION_STATUS" == "pending" ]]; do
                local CHALLENGE="$(acme.request.get_challenge "$AUTHORIZATION_RESPONSE" "http-01")"
                local TOKEN="$(acme.json.get_value "$CHALLENGE" "token")"
                RESPONSE_FILE_PATH="$DOMAIN_WELL_KNOWN_PATH/${TOKEN//[^A-Za-z0-9_-]/}"

                if [[ ! -f "$RESPONSE_FILE_PATH" ]]; then
                    echo "Build response for the pending challenge."
                    local JWK_THUMBPRINT="$(acme.request.get_json_web_key_thumbprint "$ACCOUNT_PRIVATE_KEY_FILE")"
                    local KEY_AUTHORIZATION="${TOKEN}.${JWK_THUMBPRINT}"

                    echo "$KEY_AUTHORIZATION" > "$RESPONSE_FILE_PATH"

                    echo "Notify server that response is ready"
                    local CHALLENGE_ENDPOINT="$(acme.json.get_value "$CHALLENGE" "url")"
                    local CHALLENGE_NOTIFY_RESPONSE="$(acme.request.send "$CHALLENGE_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "{}")"
                    NONCE="$(acme.request.get_nonce "$CHALLENGE_NOTIFY_RESPONSE")"
                fi

                sleep 2

                AUTHORIZATION_RESPONSE="$(acme.request.send "$AUTHORIZATION_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE")"
                NONCE="$(acme.request.get_nonce "$AUTHORIZATION_RESPONSE")"
                AUTHORIZATION_STATUS="$(acme.request.get_authorization_status "$AUTHORIZATION_RESPONSE")"
                echo "Authorization status is $AUTHORIZATION_STATUS."
            done

            rm -f "$RESPONSE_FILE_PATH"
        done
    fi

    local ORDER_ENDPOINT="$(acme.request.get_location "$ORDER_RESPONSE")"

    while [[ "$ORDER_STATUS" != "ready" && "$ORDER_STATUS" != "processing" && "$ORDER_STATUS" != "valid" ]]; do
        echo "Order status is $ORDER_STATUS. Poll until order status is ready or processing or valid."
        sleep 2

        ORDER_RESPONSE="$(acme.request.send "$ORDER_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE")"
        NONCE="$(acme.request.get_nonce "$ORDER_RESPONSE")"
        ORDER_STATUS="$(acme.json.get_value "$(acme.http.extract_body "$ORDER_RESPONSE")" "status")"
        echo "Order status is $ORDER_STATUS"
    done

    if [[ "$ORDER_STATUS" != "processing" && "$ORDER_STATUS" != "valid" ]]; then
        echo "Submit finalization request."
        local FINALIZE_ENDPOINT="$(acme.json.get_value "$ORDER_RESPONSE_BODY" "finalize")"

        local CERTIFICATE_SIGNING_REQUEST="$(acme.request.get_csr "$DOMAIN_CONFIG_FILE")"
        local PAYLOAD_JSON="$(printf '{"csr":"%s"}' "$CERTIFICATE_SIGNING_REQUEST")"

        ORDER_RESPONSE="$(acme.request.send "$FINALIZE_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$PAYLOAD_JSON")"
        NONCE="$(acme.request.get_nonce "$ORDER_RESPONSE")"
        ORDER_STATUS="$(acme.json.get_value "$(acme.http.extract_body "$ORDER_RESPONSE")" "status")"
    fi

    while [[ "$ORDER_STATUS" != "valid" ]]; do
        echo "Order status is $ORDER_STATUS. Poll until order status is valid."
        sleep 2

        ORDER_RESPONSE="$(acme.request.send "$ORDER_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE")"
        NONCE="$(acme.request.get_nonce "$ORDER_RESPONSE")"
        ORDER_STATUS="$(acme.json.get_value "$(acme.http.extract_body "$ORDER_RESPONSE")" "status")"
        echo "Order status is $ORDER_STATUS"
    done

    echo "Downloading certificate."
    local CERTIFICATE_ENDPOINT="$(acme.json.get_value "$(acme.http.extract_body "$ORDER_RESPONSE")" "certificate")"

    CERTIFICATE_RESPONSE="$(acme.request.send "$CERTIFICATE_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE")"

    CERTIFICATE="$(acme.http.extract_body "$CERTIFICATE_RESPONSE")"

    if [[ "$(echo "$CERTIFICATE" | head -n1)" == "-----BEGIN CERTIFICATE-----" ]]; then
        echo "Writing certificate to disk."
        echo "$CERTIFICATE" > "$DOMAIN_CERTIFICATE_FILE"
    else
        echo "Download seems corrupted. Not writing to disk!"
        echo "---BEGIN ECHO OF RESPONSE---"
        echo "$CERTIFICATE"
        echo "---END ECHO OF RESPONSE---"
    fi

    echo "Done."
}

acme.api.register_account () {
    local DIRECTORY_ENDPOINT="$1"
    local ACCOUNT_PRIVATE_KEY_FILE="$2"
    local ACCOUNT_KEY_ID_FILE="$3"
    local ACCOUNT_EMAIL="$4"

    acme.api.verify_prerequisites

    local DIRECTORY_RESPONSE="$(acme.request.get_directory_response "$DIRECTORY_ENDPOINT")"
    local NONCE="$(acme.request.get_new_nonce "$DIRECTORY_RESPONSE")"
    local NEW_ACCOUNT_ENDPOINT="$(acme.request.get_endpoint "$DIRECTORY_RESPONSE" "newAccount")"

    local PAYLOAD_JSON="$(printf '{"termsOfServiceAgreed":true,"contact":["mailto:%s"]}' "$ACCOUNT_EMAIL")"

    local NEW_ACCOUNT_RESPONSE="$(acme.request.send "$NEW_ACCOUNT_ENDPOINT" "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$PAYLOAD_JSON")"

    local ACCOUNT_KEY_ID="$(acme.request.get_location "$NEW_ACCOUNT_RESPONSE")"
    echo "$ACCOUNT_KEY_ID" > "$ACCOUNT_KEY_ID_FILE"
    chmod 0600 "$ACCOUNT_KEY_ID_FILE"

    echo "$(acme.http.extract_body "$NEW_ACCOUNT_RESPONSE")"

#    # TODO: verify HTTP status code is 201
#    # TODO: verify JSON status is "valid"
}
