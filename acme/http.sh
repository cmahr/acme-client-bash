#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ACME_HTTP_SEPARATOR="^\r$"  # https://www.rfc-editor.org/rfc/rfc7230#section-3

acme.http.extract_body () {
    local RESPONSE="$1"

    echo "$RESPONSE" | awk '/'$ACME_HTTP_SEPARATOR'/ { seen=1; next }   seen != 0 { print }'
}

acme.http.extract_header () {
    local RESPONSE="$1"
    local HEADER_KEY="$2"

    local HEADER_ROW="$(echo "$RESPONSE" | awk '/'$ACME_HTTP_SEPARATOR'/ { exit }   { print }' | grep -i "$HEADER_KEY")"
    local SANITIZED_VALUE="$(echo "${HEADER_ROW#*: }" | sed -e 's/[[:cntrl:]]//')"

    echo "$SANITIZED_VALUE"
}

acme.http.get () {
    local URL="$1"

    curl --include --silent "$URL"
}

acme.http.head () {
    local URL="$1"
    local HEADER_KEY="$2"

    local HEADERS="$(curl --head --silent "$URL")"

    if [[ ! -z "$HEADER_KEY" ]]; then
        acme.http.extract_header "$HEADERS" "$HEADER_KEY"
    else
        echo "$HEADERS"
    fi
}

acme.http.post () {
    local URL="$1"
    local DATA="$2"

    curl --request POST --header "Content-Type: application/jose+json" --data "$DATA" --include --silent "$URL"
}
