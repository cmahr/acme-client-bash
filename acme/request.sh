#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ACME_DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null && pwd )"

. "$ACME_DIRECTORY/base64.sh" || true || . ./base64.sh
. "$ACME_DIRECTORY/http.sh" || true || . ./http.sh
. "$ACME_DIRECTORY/json.sh" || true || . ./json.sh
. "$ACME_DIRECTORY/pkc.sh" || true || . ./pkc.sh


acme.request.get_authorization_status () {
    local RESPONSE="$1"

    local RESPONSE_BODY="$(acme.http.extract_body "$RESPONSE")"
    local ALL_STATUS="$(acme.json.get_value "$RESPONSE_BODY" "status")"

    echo "$ALL_STATUS" | head -n1
}

acme.request.get_challenge () {
    local RESPONSE="$1"
    local TYPE="$2"

    local RESPONSE_BODY="$(acme.http.extract_body "$RESPONSE")"
    acme.json.get_simple_object_containing "$RESPONSE_BODY" "$TYPE"
}

acme.request.get_csr () {
    local DOMAIN_CONFIG_FILE="$1"

    local CERTIFICATE_SIGNING_REQUEST="$(acme.pkc.get_csr_base64 "$DOMAIN_CONFIG_FILE")"
	acme.base64.make_urlsafe "$CERTIFICATE_SIGNING_REQUEST"
}

acme.request.get_directory_response () {
    local DIRECTORY_ENDPOINT="$1"

	acme.http.get "$DIRECTORY_ENDPOINT"
}

acme.request.get_endpoint () {
    local DIRECTORY_RESPONSE="$1"
    local ENDPOINT_KEY="$2"

    local RESPONSE_BODY="$(acme.http.extract_body "$DIRECTORY_RESPONSE")"

    acme.json.get_value "$RESPONSE_BODY" "$ENDPOINT_KEY"
}

acme.request.get_json_web_key () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"

    local EXPONENT_HEX="$(acme.pkc.get_public_exponent_hex "$ACCOUNT_PRIVATE_KEY_FILE")"
    local MODULUS_HEX="$(acme.pkc.get_modulus_hex "$ACCOUNT_PRIVATE_KEY_FILE")"

    local EXPONENT_BASE64=$(acme.base64.hex_to_urlsafe_base64 "$EXPONENT_HEX")
    local MODULUS_BASE64=$(acme.base64.hex_to_urlsafe_base64 "$MODULUS_HEX")

    printf '{"e":"%s","kty":"RSA","n":"%s"}' "$EXPONENT_BASE64" "$MODULUS_BASE64"
}

acme.request.get_json_web_key_thumbprint () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"

    JSON_WEB_KEY="$(acme.request.get_json_web_key "$ACCOUNT_PRIVATE_KEY_FILE")"
    acme.base64.make_urlsafe "$(echo -n "$JSON_WEB_KEY" | openssl dgst -sha256 -binary | base64 -w0)"
}

acme.request.get_location () {
    local RESPONSE="$1"

    acme.http.extract_header "$RESPONSE" "Location"
}

acme.request.get_new_nonce () {
    local DIRECTORY_RESPONSE="$1"

    NEW_NONCE_ENDPOINT="$(acme.request.get_endpoint "$DIRECTORY_RESPONSE" "newNonce")"
    acme.http.head "$NEW_NONCE_ENDPOINT" "Replay-Nonce"
}

acme.request.get_nonce () {
    local RESPONSE="$1"

    acme.http.extract_header "$RESPONSE" "Replay-Nonce"
}

acme.request.get_protected_header () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"
    local ACCOUNT_KEY_ID_FILE="$2"
    local NONCE="$3"
    local URL="$4"

    local KEY_ATTRIBUTE; local KEY_VALUE
    if [[ ! -f "$ACCOUNT_KEY_ID_FILE" ]]; then
        KEY_ATTRIBUTE="jwk"
        KEY_VALUE="$(acme.request.get_json_web_key "$ACCOUNT_PRIVATE_KEY_FILE")"
    else
        KEY_ATTRIBUTE="kid"
        KEY_VALUE="\"$(head -n1 "$ACCOUNT_KEY_ID_FILE")\""
    fi

    local HEADER_JSON="$(printf '{"alg":"RS256","%s":%s,"nonce":"%s","url":"%s"}' "$KEY_ATTRIBUTE" "$KEY_VALUE" "$NONCE" "$URL")"
    acme.base64.urlsafe_base64 "$HEADER_JSON"
}

acme.request.get_request_body () {
    local PROTECTED_HEADER="$1"
    local PAYLOAD="$2"
    local SIGNATURE="$3"

    printf '{"protected":"%s","payload":"%s","signature":"%s"}' "$PROTECTED_HEADER" "$PAYLOAD" "$SIGNATURE"
}

acme.request.get_signature () {
	local ACCOUNT_PRIVATE_KEY_FILE="$1"
	local PROTECTED_HEADER="$2"
	local PAYLOAD="$3"

	local SIGNATURE="$(acme.pkc.sign_base64 "$ACCOUNT_PRIVATE_KEY_FILE" "${PROTECTED_HEADER}.${PAYLOAD}")"
	acme.base64.make_urlsafe "$SIGNATURE"
}

acme.request.send () {
    local ENDPOINT="$1"
    local ACCOUNT_PRIVATE_KEY_FILE="$2"
    local ACCOUNT_KEY_ID_FILE="$3"
    local NONCE="$4"
    local PAYLOAD_JSON="$5"

    local PROTECTED_HEADER="$(acme.request.get_protected_header "$ACCOUNT_PRIVATE_KEY_FILE" "$ACCOUNT_KEY_ID_FILE" "$NONCE" "$ENDPOINT")"

    local PAYLOAD=""
    if [[ ! -z "$PAYLOAD_JSON" ]]; then
        PAYLOAD=$(acme.base64.urlsafe_base64 "$PAYLOAD_JSON")
    fi

    local SIGNATURE="$(acme.request.get_signature "$ACCOUNT_PRIVATE_KEY_FILE" "$PROTECTED_HEADER" "$PAYLOAD")"

    local REQUEST_BODY="$(acme.request.get_request_body "$PROTECTED_HEADER" "$PAYLOAD" "$SIGNATURE")"

    acme.http.post "$ENDPOINT" "$REQUEST_BODY"
}
