#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

acme.json.get_simple_array_values () {
    local JSON="$1"
    local KEY="$2"

    LINE_NUMBER_KEY="$(echo "$JSON" | grep -n "$KEY" | sed 's/://g' | awk '{ print $1 }')"
    REL_LINE_NUMBER_START="$(echo "$JSON" | tail -n+$LINE_NUMBER_KEY | grep -n "\[" | sed 's/://g' | awk '{ print $1 }')"
    REL_LINE_NUMBER_END="$(echo "$JSON" | tail -n+$LINE_NUMBER_KEY | grep -n "\]" | sed 's/://g' | awk '{ print $1 }')"

    LINE_NUMBER_START="$(($LINE_NUMBER_KEY+$REL_LINE_NUMBER_START))"
    LINE_COUNT="$(($REL_LINE_NUMBER_END-$REL_LINE_NUMBER_START-1))"

    echo "$JSON" | tail -n+$LINE_NUMBER_START | head -n$LINE_COUNT | sed 's/^\s*"//;s/,\s*$//;s/"\s*$//'
}

acme.json.get_simple_object_containing () {
    local JSON="$1"
    local NEEDLE="$2"

    LINE_NUMBER_NEEDLE="$(echo "$JSON" | grep -n "$NEEDLE" | sed 's/://g' | awk '{ print $1 }')"

    REL_LINE_NUMBER_END="$(echo "$JSON" | tail -n+$LINE_NUMBER_NEEDLE | grep -m1 -n '}' | sed 's/://g' | awk '{ print $1 }')"
    REL_LINE_NUMBER_START="$(echo "$JSON" | head -n$LINE_NUMBER_NEEDLE | tac | grep -m1 -n '{' | sed 's/://g' | awk '{ print $1 }')"

    LINE_NUMBER_START="$(($LINE_NUMBER_NEEDLE-$REL_LINE_NUMBER_START+1))"
    LINE_COUNT="$(($REL_LINE_NUMBER_START+$REL_LINE_NUMBER_END-1))"

    echo "$JSON" | tail -n+$LINE_NUMBER_START | head -n$LINE_COUNT
}

acme.json.get_value () {
    local JSON="$1"
    local KEY="$2"

	local VALUE=$(echo "$JSON" | grep "$KEY" | sed 's/"//g;s/,//g' | awk '{ print $2 }')
	acme.json.remove_control_characters "$VALUE"
}

acme.json.remove_control_characters () {
	echo "$1" | sed -e 's/[[:cntrl:]]//'
}
