#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

acme.base64.base64 () {
    echo "$1" | base64 -w0
}

acme.base64.hex_to_base64 () {
	echo "$1" | xxd -r -p | base64 -w0
}

acme.base64.hex_to_urlsafe_base64 () {
    acme.base64.make_urlsafe "$(acme.base64.hex_to_base64 "$1")"
}

acme.base64.make_urlsafe () {
    echo "$1" | sed 's|+|-|g;s|/|_|g;s|=||g'
}

acme.base64.urlsafe_base64 () {
    acme.base64.make_urlsafe "$(acme.base64.base64 "$1")"
}
