#!/usr/bin/env bash
# Copyright 2019 Carsten E. Mahr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

acme.pkc.build_csr_san_list () {
    local SAN_LIST

    for domain in "${DOMAIN_SANS[@]}"; do
        SAN_LIST+="DNS:$domain,"
    done

    printf "${SAN_LIST::-1}"
}

acme.pkc.build_csr_subject () {
    local SUBJECT

    [[ ! -z "$DOMAIN_COUNTRY" ]] && SUBJECT+="/C=$DOMAIN_COUNTRY"
    [[ ! -z "$DOMAIN_STATE" ]] && SUBJECT+="/ST=$DOMAIN_STATE"
    [[ ! -z "$DOMAIN_CITY" ]] && SUBJECT+="/L=$DOMAIN_CITY"
    [[ ! -z "$DOMAIN_ORGANIZATION_NAME" ]] && SUBJECT+="/O=$DOMAIN_ORGANIZATION_NAME"
    [[ ! -z "$DOMAIN_ORGANIZATION_UNIT" ]] && SUBJECT+="/OU=$DOMAIN_ORGANIZATION_UNIT"
    [[ ! -z "$DOMAIN_COMMON_NAME" ]] && SUBJECT+="/CN=$DOMAIN_COMMON_NAME"
    [[ ! -z "$DOMAIN_EMAIL_ADDRESS" ]] && SUBJECT+="/emailAddress=$DOMAIN_EMAIL_ADDRESS"

    printf "$SUBJECT"
}

acme.pkc.get_csr_base64 () {
    DOMAIN_CONFIG_FILE="$1"

    . "$DOMAIN_CONFIG_FILE"

    SUBJECT="$(acme.pkc.build_csr_subject)"

    if [[ "${#DOMAIN_SANS[@]}" != "0" ]]; then
        openssl req -utf8 -new -sha256 -key "$DOMAIN_PRIVATE_KEY_FILE" -subj "$SUBJECT" -outform DER -reqexts SAN \
            -config <(
                cat /etc/ssl/openssl.cnf <(
                    printf "[SAN]\nsubjectAltName=%s" "$(acme.pkc.build_csr_san_list)"
                )
            ) | base64 -w0
    else
        openssl req -utf8 -new -sha256 -key "$DOMAIN_PRIVATE_KEY_FILE" -subj "$SUBJECT" -outform DER | base64 -w0
    fi
}

acme.pkc.get_modulus_hex () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"

    acme.pkc.get_value_hex "$ACCOUNT_PRIVATE_KEY_FILE" "3"
}

acme.pkc.get_public_exponent_hex () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"

    acme.pkc.get_value_hex "$ACCOUNT_PRIVATE_KEY_FILE" "4"
}

acme.pkc.get_value_hex () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"
    local LINE_NUMBER="$2"

	openssl asn1parse -in "$ACCOUNT_PRIVATE_KEY_FILE" | sed 's|:||g' | awk 'NR=='${LINE_NUMBER}' { print $7 }'
}

acme.pkc.sign_base64 () {
    local ACCOUNT_PRIVATE_KEY_FILE="$1"
    local MESSAGE="$2"

	echo -n "$MESSAGE" | openssl dgst -sha256 -binary -sign "$ACCOUNT_PRIVATE_KEY_FILE" | base64 -w0
}
